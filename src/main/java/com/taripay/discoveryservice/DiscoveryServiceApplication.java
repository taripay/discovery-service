package com.taripay.discoveryservice;

import com.netflix.discovery.EurekaClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.annotation.Lazy;

@SpringBootApplication
@EnableEurekaServer
public class DiscoveryServiceApplication {
    
         @Autowired
    @Lazy
    private EurekaClient eurekaClient;

	public static void main(String[] args) {
		SpringApplication.run(DiscoveryServiceApplication.class, args);
	}

}
